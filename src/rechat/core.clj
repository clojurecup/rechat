(ns rechat.core
  (:gen-class)
  (:require [aleph.http :as http]
    [compojure.core :as compojure :refer [GET]]
    [ring.middleware.params :as params]
    [compojure.route :as route]
    [byte-streams :as bs]
    [manifold.stream :as s]
    [manifold.deferred :as d]
    [manifold.bus :as bus]
    [clojure.core.async :as a]))

(def non-websocket-request
  {:status 400
   :headers {"content-type" "application/text"}
   :body "Expected a websocket request."})

(def chatrooms (bus/event-bus))

(defn rand-name []
  (str
    (rand-nth ["annoying" "spooky" "lame" "vivid" "insane" "curious" "smart" "aggressive"
               "aloof" "arrogant" "belligerent" "big-headed" "bitchy" "boastful" "bone-idle"
               "boring" "bossy" "callous" "cantankerous" "careless" "changeable"
               "clinging" "compulsive" "conservative" "cowardly" "cruel" "cunning" "cynical"])
    " "
    (rand-nth ["alpaca" "ant" "ape" "armadillo" "bear" "bee" "crow" "duck" "eel" "penguin" "llama"
               "lizard" "goat" "sheep" "zebra" "snake"])))

(defn chat-handler
  [req]
  (d/let-flow [conn (d/catch
                      (http/websocket-connection req)
                      (fn [_] nil))]
              (if-not conn
                ;; if it wasn't a valid websocket handshake, return an error
                non-websocket-request
                ;; otherwise, take the first two messages, which give us the chatroom and name
                (d/let-flow #_[room (s/take! conn)
                             name (s/take! conn)]
                            [room "Hello"
                             name (rand-name)]
                            ;; take all messages from the chatroom, and feed them to the client
                            (s/connect
                              (bus/subscribe chatrooms room)
                              conn)
                            ;; take all messages from the client, prepend the name, and publish it to the room
                            (s/consume
                              #(bus/publish! chatrooms room %)
                              (->> conn
                                   (s/map #(str name ": " %))
                                   (s/buffer 100)))))))

(defn -main
  [& args]
  (http/start-server chat-handler {:port 8080}))
