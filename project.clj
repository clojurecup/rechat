(defproject rechat "0.1.0-SNAPSHOT"
  :description "Experimental repl chat"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/core.async "0.2.374"]
                 [aleph "0.4.0"]
                 [compojure "1.4.0"]]

  :main ^:skip-aot rechat.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
